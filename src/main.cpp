#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <chrono>

#include "MyComparator.h"

int main() {
  static constexpr int vector_size = 1000000;
  srand(17); // seed rand to a constant to make it generate the same sequence each time
  std::vector<int> intVec;
  intVec.reserve(vector_size);
  std::generate_n(std::back_inserter(intVec), vector_size, rand);

  auto t1 = std::chrono::system_clock::now();
  std::sort(intVec.begin(), intVec.end(), my_less_than);
  auto t2 = std::chrono::system_clock::now();

//  for (const int i : intVec)
//    std::cout << i << '\n';

  std:: cout << "it took: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() << "ms\n";

  return 0;
}